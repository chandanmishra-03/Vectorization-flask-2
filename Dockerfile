FROM python:3.6-jessie

WORKDIR /app

COPY ./app /app

RUN pip install --no-cache-dir -r requirements.txt

#CMD ["gunicorn", "app:app", "--config=config.py"]
CMD ["python", "app.py"]